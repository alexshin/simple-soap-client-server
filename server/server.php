<?php

require_once 'SoapFunctions.php';
require_once 'vendor/autoload.php';

use WSDL\WSDLCreator;

if (isset($_GET['wsdl'])) {
    $wsdl = new WSDL\WSDLCreator('SoapFunctions', 'http://soap.local/server/server.php');
    $wsdl->setNamespace("http://foo.bar/");
    $wsdl->renderWSDL();
    exit;
}

$options= array('uri'=>'http://localhost/everth');
$server = new SoapServer(null, $options);
$server->setClass('SoapFunctions');
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])){
	if ($_SERVER['PHP_AUTH_USER'] == "username" && $_SERVER['PHP_AUTH_PW'] == "password"){
		$server->handle();	
	}
	else {
		echo "Wrong username or password";
	}
}
else {
	echo "Empty credentials";
}