<?php
class SoapFunctions {

	/**
	 * @WebMethod
	 * @desc Returns pong string value
	 * @return string $return
	 */
	public function ping(){
		return "pong";
	}

	/**
	 * @WebMethod
	 * @desc Returns Concatenation of strings
	 * @param string $word1
	 * @param string $word2
	 * @return string $return
	 */
	public function helloWorld($word1, $word2){
		return implode(" ", [$word1, $word2]);
	}
}