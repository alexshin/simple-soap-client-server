<?php
require_once('client.php');
require_once('functions.php');

try{
	if (count($argv) < 4){
		throw new Exception("Not enough arguments. Expected: <url> <login> <password>");
	}

	$url = $argv[1];
	$login = $argv[2];
	$password = $argv[3];

	$client = new TestSoapClient($url, $login, $password);
}
catch (Exception $e){
	echo $e->getMessage();
	exit(1);
}

?>

Examine of SOAP-client
-----------------------

- SOAP-server URI: <?=$url?>
- SOAP-server credentials: <?=$login?>:<?=$password?>

------------------
Test ping function
------------------
Execute ping() function on the server.
Expected response (string) "pong"

<?php var_dump(ping($client->getClient())); ?>

------------------------
Test helloWorld function
------------------------
Execute helloWorld((string) $word1, (string) $word2)
Expected response (string) $word1 + " " + $word2

<?php var_dump(helloWorld($client->getClient(), 'Hello', 'word')); ?>