<?php

	class TestSoapClient {

		private $client = null;

		function __construct($url, $login, $password){
			
			try {
				$this->client = new SoapClient($url, [
					'login' => $login,
					'password' => $password,
					'soap_version' => SOAP_1_2,
					'cache_wsdl' => WSDL_CACHE_NONE,
					'exceptions' => true,
					'trace' => 1
				]);
				
			}
			catch(SoapFault $e){
				throw new Exception("SOAP server couldn't start: ".$e->getMessage());
			}
		}

		public function getClient(){
			if (is_null($this->client)){
				throw new Exception("Instance of SOAP-client is not exist");
			}

			return $this->client;
		}
	}