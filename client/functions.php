<?php

function ping($client){
	$response = $client->ping();
	return $response;
}


function helloWorld($client, $word1, $word2){
	$response = $client->helloWorld($word1, $word2);
	return $response;
}